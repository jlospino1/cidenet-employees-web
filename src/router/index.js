import Vue from 'vue'
import VueRouter from 'vue-router'

import Employee from '../pages/employees/Employees.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Employees',
    component: Employee
  },
  // otherwise redirect to home
  { path: '*', redirect: '/' }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;