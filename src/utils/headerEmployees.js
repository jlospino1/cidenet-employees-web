export default [
    {
        text: 'Primer nombre',
        align: 'left',
        sortable: true,
        value: 'firstName'
      },
      {
        text: 'Segundo nombre',
        align: 'left',
        sortable: true,
        value: 'secondName'
      },
      {
        text: 'Primer apellido',
        align: 'left',
        sortable: true,
        value: 'lastName'
      },
      {
        text: 'Primer apellido',
        align: 'left',
        sortable: true,
        value: 'otherName'
      },
      {
        text: 'Tipo de documento',
        align: 'left',
        sortable: true,
        value: 'documentType'
      },
      {
        text: 'Número de documento',
        align: 'left',
        value: 'documentNumber'
      },
      {
        text: 'Pais',
        align: 'left',
        value: 'country'
      },
      {
        text: 'Correo electrónico',
        align: 'left',
        value: 'email'
      },
      {
        text: 'Estado',
        align: 'left',
        value: 'state'
      },
      { text: 'Acciones', value: 'actions', sortable: false },
]